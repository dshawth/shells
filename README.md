# Shells

> My (POSIX) shell snippets

## Table of Contents

- [Iteration](#iteration)
- [Multi-Processing](#multi-processing)

## Iteration

### Files/Folders in a Directory

```sh
for item in /path/*; do
    echo "$item"
done
```

### Files with Extension in a Directory

```sh
for file in "$folder"/*.extension; do
    echo "$file"
done
```

### Lines in a File

```sh
while read -r line || [ -n "$line" ]; do
    # above || allows non-posix files that are missing the trailing \n
    # without it, the last line is lost in non-posix files
    echo "$line"
done < "$file"
```

## Multi-Processing

```sh
max='4' # number jobs
for item in $collection; do
    # if tasks are visible in `jobs -p`
    while [ "$(jobs -p | wc -l)" -ge "$max" ]; do
        echo 'jobs full waiting a second'
        sleep 1
    done
    (   
        echo 'do things in parallel here'
        sleep 5
    ) &
done
```

```sh
max='4' # number of jobs
while read -r line || [ -n "$line" ]; do
    # if tasks are not tracked by `jobs -p`
    while [ "$(ps axo user:20,command | grep $(whoami) | grep -c $cmd)" -ge "$threads" ]; do
        echo 'jobs full waiting a second'
        sleep 1
    done
    (
        echo 'do things in parallel here'
        cmd='sleep'
        sleep 5
    ) &
done < "$file"
```
